import React from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import useSWR, { useSWRConfig } from "swr";

const ProductList = () => {
  const { mutate } = useSWRConfig;
  const fetcher = async () => {
    const respone = await axios.get("http://localhost:5000/products");
    return respone.data;
  };
  const { data } = useSWR("products", fetcher);
  if (!data) return <h2>Loading...</h2>;

  const deleteProduct = async (productId) => {
    await axios.delete(`http://localhost:5000/products/${productId}`);
    mutate("products");
  };

  return (
    <div className="flex flex-col mt-5">
      <div className="w-full">
        <Link
          to="/add"
          className="px-4 py-2 font-bold text-white bg-green-500 border rounded-lg hover:bg-green-700 border-slate-400"
        >
          Add New
        </Link>
        <div className="relative mt-5 rounded-lg shadow">
          <table className="w-full text-sm text-left text-gray-500">
            <thead className="text-xs text-gray-700 uppercase bg-gray-100">
              <tr>
                <th className="px-1 py-3 text-center">No</th>
                <th className="px-6 py-3 ">Product Name</th>
                <th className="px-6 py-3 t">Pr</th>
                <th className="px-1 py-3 text-center">Action</th>
              </tr>
            </thead>
            <tbody>
              {data.map((product, index) => (
                <tr className="bg-white border-b" key={product.id}>
                  <td className="px-1 py-3 text-center">{index + 1}</td>
                  <td className="px-6 py-3 ">{product.name}</td>
                  <td className="px-6 py-3 t">{product.price}</td>
                  <td className="px-1 py-3 text-center">
                    <Link
                      to={`/edit/${product.id}`}
                      className="px-3 py-1 mr-1 font-medium text-white bg-blue-400 rounded-lg hover:bg-blue-600"
                    >
                      Edit
                    </Link>
                    <button
                      onClick={() => deleteProduct(product.id)}
                      className="px-3 py-1 mr-1 font-medium text-white bg-red-400 rounded-lg hover:bg-red-500"
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default ProductList;
